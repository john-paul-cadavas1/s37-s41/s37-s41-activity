const Product = require("../models/Product");


// Create Product
module.exports.createProduct = (reqBody) => {
	console.log(reqBody)
	return Product.findOne({"name": reqBody.name}).then(result => {
		console.log(result)
		if(result === null){
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})

			return newProduct.save().then((product, err) => {
				if(err){
					console.log(err)
					return false
				} else{
					console.log(product)
					return "Product successfully added"
				}
			})
		} else {
			return `Product already exists`
		}
	})

	
}


// retrieve all products
module.exports.getAllProducts = () => {

	return Product.find({"isActive": true}).then(result => {
		const getAllProducts = []
		for(i = 0; i < result.length; i++){
			getAllProducts.push({
				name: result[i].name,
				description: result[i].description,
				price: result[i].price,
				isActive: result[i].isActive,
				createdOn: result[i].createdOn
			})
			console.log(getAllProducts)
		}
		return getAllProducts
	})
}

// retrieve a single product
module.exports.getProduct = (productId) => {

	return Product.findById(productId).then(result => {
		if(result === null){
			return "Product not found"
		} else{
			const product = {
				name: result.name,
				description: result.description,
				price: result.price,
				isActive: result.isActive,
				createdOn: result.createdOn
			}
			return product
		}
	})
}

// Updating a product
module.exports.updateProduct = (data) => {
	console.log(data)
	return Product.findById(data.productId).then(result => {
		console.log(result)
		
		if(result === null){
			return `Product does not exists`
		} else {
			result.name = data.updatedProduct.name,
			result.description = data.updatedProduct.description,
			result.price = data.updatedProduct.price

			return result.save().then((result, error) => {
				if(error){
					return false
				} else{
					const product = {
						name: result.name,
						description: result.description,
						price: result.price,
						isActive: result.isActive,
						createdOn: result.createdOn
					}
					return product
				}
			})
		}
	})
}

// Archive Product
module.exports.archiveProduct = (productId) => {

	return Product.findById(productId).then(result => {
		console.log(result)
		result.isActive = false;

		return result.save().then((archProduct, error) => {
			if(error){
				console.log(error)
				return false
			} else{
				console.log(archProduct)
				return "Active status changed"
			}
		})
	})

}