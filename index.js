const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4000;

// Access Routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")


const app = express();
//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin123:admin123@course-booking.e4eg3.mongodb.net/capstone2_coffeeShop?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'Connection Error'))
db.once('open', () => console.log('Now connected to MongoDB Atlas'))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);







app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});