const express = require("express");
const router = express();
const productControllers = require("../controllers/productControllers")
const auth = require("../auth")


// Route to create a product
router.post("/createProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization).isAdmin
	
	console.log(userData)
	if(userData === true){
		productControllers.createProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`You are not an admin`)
	}
})


// Route to retrieve all products
router.get("/", (req, res) => {
	productControllers.getAllProducts().then(resultFromController =>
		res.send(resultFromController));
})

// Route to retrieve a single product
router.get("/:productId", (req, res) => {
	console.log(req.params)
	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedProduct: req.body
	}

	if(data.isAdmin == true){
		productControllers.updateProduct(data).then(resultFromController => res.send(resultFromController));
	} else{
		res.send(`You are not an admin`)
	}
})

// Route to Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin)
	console.log(req.params.productId)

	if(isAdmin){
		productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
	} else{
		res.send(`You are not an admin`)
	}

})



module.exports = router;