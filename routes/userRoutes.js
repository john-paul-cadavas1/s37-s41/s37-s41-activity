const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");
const Product = require("../models/Product");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for setting a user to admin(Admin only)
router.post("/:userId/setAsAdmin", auth.verify, (req, res) => {
	console.log(req.params)
	let data = {
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	} 
	console.log(data)
	if(data.isAdmin === true){
		userController.setAsAdmin(data.userId).then(resultFromController => res.send(resultFromController));
	} else{
		res.send("You are not an admin")
	}
})

// Route for creating an order
router.post("/checkout", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data.isAdmin)
	if(data.isAdmin === true){
		res.send(`You are not allowed to order`)
	} else{
		userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	}

})

// Route to retrieve all orders
router.get("/orders", auth.verify, (req, res) => {

	 userAdmin = auth.decode(req.headers.authorization).isAdmin

	 if(userAdmin == true){
	 	userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	 } else {
	 	res.send(`You are not an admin`)
	 }
})

// Route to get Authenticated User’s Orders
router.get("/myOrders", auth.verify, (req, res) => {

	data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	console.log(data)
	if(data.isAdmin){
		res.send(`Access denied`)
	} else{
		userController.getOrders(data.userId).then(resultFromController => res.send(resultFromController));
	}
})



module.exports = router;