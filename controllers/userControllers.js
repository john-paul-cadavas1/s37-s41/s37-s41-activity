const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

// User Registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({"email": reqBody.email}).then(result => {
		console.log(result)
		if(result === null){
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10) 
			})

			return newUser.save().then((user, error) => {
				if(error){
					console.log(error)
					return false
				} else {
					console.log(user)
					return `${user.email} is successfully registered`
				}
			})
		} else {
			return `Email is already taken`
		}
	})
}

// User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if(result === null){
			return `Invalid email`
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return `Incorrect password`
			}
		}
	})
}

// Set user as admin
module.exports.setAsAdmin = (reqBody) => {

	console.log(reqBody);
	return User.findById(reqBody).then(result => {
		if(result.isAdmin === true){
			return `${result.email} is already an admin`
		} else {
			result.isAdmin = true

			return result.save().then((res, err) => {
				if(err){
					return false
				} else {
					return `${result.email} is set to admin successfully`
				}
			})
		}
	})
}

// Creating an order
module.exports.createOrder = async (data) => {

	console.log(data)

	let isUserUpdated = await User.findById(data.userId).then(user => {
		console.log(user)
		return Product.findById(data.productId).then(result => {
			console.log(result)
			console.log(user)
			console.log(data)
			if(result.isActive == true){
				user.orders.push({
					productId: result.id,
					productName: result.name,
					totalAmount: result.price * data.quantity
				})
				return user.save().then((user, error) => {
					console.log(user)
					if(error){
						return false
					} else {
						return true
					}
				})
			} else {
				return false
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		console.log(product)

		return User.findById(data.userId).then(result => {
			console.log(result)

			if(product.isActive == true){
				product.userOrders.push({
					userId: result.id,
					userEmail: result.email,
				})

				return product.save().then((product, error) => {
					console.log(product)
					if(error){
						return false
					} else{
						return true
					}
				})
			} else {
				return false
			}
		})
	})

	let isOrderUpdated = await User.findById(data.userId).then(user => {
		return Product.findById(data.productId).then(result => {
			console.log(result)
			console.log(`${result.price}, ${data.quantity}`)

			if(result.isActive == true) {
				let newOrder = new Order({
					userId: user.id,
					userEmail: user.email,
					totalAmount: result.price * data.quantity,
					items: [
						{
							productId: result.id,
							productName: result.name,
							productPrice: result.price,
							quantity: data.quantity
						}
					]
				})

				return newOrder.save().then((order, err) => {
					if(err){
						return false
					} else{
						return true
					}
				})
			} else {
				return false
			}
		})
	})

	if(isUserUpdated && isProductUpdated && isOrderUpdated){
		return "Order successful"
	} else{
		return `Product not Available`
	}
}

// retrieve all orders
module.exports.getAllOrders = () => {

	return Order.find({}).then(result => {
		return result
	})
}

// Get Authenticated User’s Orders
module.exports.getOrders = (data) => {

	return Order.find({"userId": data}).then(result => {
		console.log(result)
		if(result == null){
			return false
		} else{
			return result
		}
	})
}