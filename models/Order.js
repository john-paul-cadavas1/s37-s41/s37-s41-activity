const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "User ID is required"]
	},
	userEmail : {
		type: String,
		required : [true, "User Email is required"]
	},
	totalAmount : {
		type : Number,
		required : [true, "Total Amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	items : [
		{
			productId : {
				type: String,
				required : [true, "Product ID is required"]
			},
			productName : {
				type : String,
				required : [true, "Product Name is required"]
			},
			productPrice : {
				type : Number,
				required : [true, "price is required"]
			},
			quantity : {
				type : Number,
				required: [true, "quantity is required"]
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema);